// Mesh methods
// https://docs.snap.com/api/lens-studio/Classes/Components#RenderMeshVisual--Methods

var sceneObject = script.getSceneObject();
var mesh = sceneObject.getFirstComponent("RenderMeshVisual");

if (global.launchParams && global.launchParams.has("shapekey_names" ) && global.launchParams.has("shapekey_values")) {
  
  var names = launchParams.getStringArray("shapekey_names" );
  var values = launchParams.getFloatArray("shapekey_values");

  for (var i = 0; i < names.length; i++) {
    mesh.setBlendShapeWeight(names[i], values[i])
  }
}
