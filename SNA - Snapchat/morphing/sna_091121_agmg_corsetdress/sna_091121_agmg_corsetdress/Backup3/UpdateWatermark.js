var sceneObject = script.getSceneObject();
var image = sceneObject.getFirstComponent("Image");
var screenTransform = sceneObject.getFirstComponent("ScreenTransform");

// set up alpha channel
if (global.launchParams && global.launchParams.has("watermark_alpha")) {
  var alpha = launchParams.getFloat("watermark_alpha");  
  var baseColor = image.mainPass.baseColor;
  image.mainPass.baseColor = new vec4(baseColor.r, baseColor.g, baseColor.b, alpha);
}

// set up watermark anchors
if (global.launchParams && global.launchParams.has("watermark_top")) {
  var anchors = screenTransform.anchors;
  anchors.top = launchParams.getFloat("watermark_top");
  anchors.right = launchParams.getFloat("watermark_right");
  anchors.bottom = launchParams.getFloat("watermark_bottom");
  anchors.left = launchParams.getFloat("watermark_left");
}

// set up custom watermark
if (global.launchParams && global.launchParams.has("watermark_string")) {
  var width = launchParams.getFloat("watermark_width");   
  var height = launchParams.getFloat("watermark_height");  
  var base64 = launchParams.getString("watermark_string");
  
  var pixels = global.toByteArray(base64);
    
  var newTex = ProceduralTextureProvider.create(width, height, Colorspace.RGBA);
  newTex.control.setPixels(0, 0, width, height, pixels);
 
  image.mainPass.baseTex = newTex;
    
} else {
  // delete the object since there is no watermark texture passed
  sceneObject.destroy();
}
