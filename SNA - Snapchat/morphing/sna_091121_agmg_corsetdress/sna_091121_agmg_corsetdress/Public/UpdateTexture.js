var sceneObject = script.getSceneObject();
var mesh = sceneObject.getFirstComponent("RenderMeshVisual");

if (global.launchParams && global.launchParams.has("texture_string")) {
  var width = launchParams.getFloat("texture_width");   
  var height = launchParams.getFloat("texture_height");  
  var base64 = launchParams.getString("texture_string");
  
  var pixels = global.toByteArray(base64);
    
  var newTex = ProceduralTextureProvider.create(width, height, Colorspace.RGBA);
  newTex.control.setPixels(0, 0, width, height, pixels);
 
  mesh.mainPass.baseTex = newTex;
}